import Link from "next/link";
import styles from "./header.module.scss";
import { ChangeEvent, useState } from "react";
type Props = {};

const Header = (props: Props) => {
  const [searchQuery, setSearchQuery] = useState<string>("");
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(e.target.value);
  };
  return (
    <header className={styles.header}>
      <h2 className={styles.logo}>Logo</h2>
      <div>
        <Link href="/">Home</Link>
        <Link href="/about">About</Link>
      </div>
      <input
        type="text"
        value={searchQuery}
        onChange={handleChange}
        placeholder="Search movies"
      />
    </header>
  );
};

export default Header;
