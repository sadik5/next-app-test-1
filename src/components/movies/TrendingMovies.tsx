import { FC, useEffect, useState } from "react";
import { getRequest } from "@/utils/http-helper";
import styles from "./movies.module.scss";
type TrendingMoviesProps = {};

const TrendingMovies: FC<TrendingMoviesProps> = ({}) => {
  useEffect(() => {
    getMovies();
  }, []);

  const [TrendIndingMovies, setTrendIndingMovies] = useState([]);

  const getMovies = async () => {
    const res = await getRequest({
      url: "https://api.themoviedb.org/3/trending/movie/day",
      method: "GET",
    });

    if (res.results.length > 0) {
      setTrendIndingMovies(res.results);
    }
  };

  return (
    <>
      <div className={styles.Movies}>
        {TrendIndingMovies.map((movie: any, index) => {
          // const imageSrc = movie.poster_path
          //   ? `https://image.tmdb.org/t/p/original${movie.poster_path}`
          //   : movie.backdrop_path
          //   ? `https://image.tmdb.org/t/p/original${movie.backdrop_path}`
          //   : "";
          return (
            <div className={styles.movieCard} key={index}>
              {/* <Image src={imageSrc} alt={movie.original_title} /> */}
              <div className={styles.movieInfo}>
                <div className={styles.movieTitle}>{movie.original_title}</div>
                <div className={styles.movieYear}>{movie.release_date}</div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default TrendingMovies;
