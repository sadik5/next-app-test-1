import axios from "axios";

export const getRequest = async ({
  url,
  method = "GET",
  body = {},
  headers = {},
  params = {},
}: {
  url: string;
  method: string;
  body?: any;
  headers?: any;
  params?: any;
}) => {
  try {
    const options = {
      method: method,
      url: url,
      params: { language: "en-US", ...params },
      headers: {
        accept: "application/json",
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0Mjg2MzE2Y2VlZmE4Y2EzMTllNzVlNDliMWViYmQxMiIsInN1YiI6IjYyZjIyNzY5MWY3NDhiMDA4MTE2NmNlZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Rb_3-DzRy_IJJBbwvs27taO9SYWHtatgMMTlLeBO3VU",
        ...headers,
      },
      data: body,
    };

    const response = await axios(options);
    if (response && response.status === 200 && response.data) {
      return response.data;
    } else return null;
  } catch (error) {
    console.error(error);
    return error;
  }
};
